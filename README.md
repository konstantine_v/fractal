# Fractal

[![Language](https://img.shields.io/badge/language-crystal-776791.svg)](https://github.com/crystal-lang/crystal)
![Status](https://img.shields.io/badge/status-WIP-blue.svg)

Generate fractals and output them to PNG format. Based on the [Fractal Ruby Gem](https://github.com/Demonstrandum/Fractal/).

*WIP - This project is still being worked on* 


## Installation

- Clone project `git clone <https://gitlab.com/MaterialFuture/fractal/>`
- `cd fractal`
- Install using `shards install`
- `crystal build src/fractal.cr`
- `mv fractal bin/`

## Usage

It'll mirror what [Fractal Ruby Gem](https://github.com/Demonstrandum/Fractal/) does with some QOL improvements and ability to extend it's functionality. Still WIP, when it's functional I'll put the docs here plus have `--help` and `help` work in the app.

## Development

- Clone project `git clone <https://gitlab.com/MaterialFuture/fractal/>`
- `cd fractal`
- Install using `shards install`
- Make changes to `src/fractal.cr`

## Contributing

1. Fork it (<https://gitlab.com/MaterialFuture/fractal/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Konstantine](https://gitlab.com/materialfuture) - creator and maintainer

## Todo

- MVP
  * [x] Setup Project
  * [x] Convert Code From Ruby to Crystal
  * [x] Setup Admiral Flags by breaking down commands
  * [ ] Setup Admiral Defaults
  * [ ] Have code be functional in the CLI
  * [ ] Have code be functional in the CLI
- Nice To Have
  * [ ] Create Tests in spec
  * [ ] Setup more output formats
  * [ ] Have Errors Log to a log file
  * [ ] Work with other data sets other than just the main two fractal types
  * [ ] Randomize flag that sets some random parameters to render